/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

/**
 * 
 * @author Jurgenmolina <jurgenmolina29@gmail.com>
 */
public class Persona implements Comparable{
    
    private long cedula;
    private String nombre;
    
    //Requisito 1: 

    public Persona() {
    }

    public Persona(long cedula, String nombre) {
        this.cedula = cedula;
        this.nombre = nombre;
    }
    
    
    public Persona(String datos) throws Exception
    {
      if(datos==null || datos.isEmpty())  
      {
          throw new Exception("Los datos estan vacios");
          
      }
      //else : no hay necesidad 
      String datosPersona[]=datos.split(",");
      if(datosPersona.length!=2)
      {
          throw new Exception("La cadena no tiene las partes requeridas para asignarse a una Persona");
      }
      
      try{
        this.cedula=Long.parseLong(datosPersona[0]);
        this.nombre=datosPersona[1];
      }catch(Exception e)
      {
          throw new Exception("dato no válido, revise que su formato sea: cedula, nombre "+e.getMessage());
      }
      // :)
     
    }
    
    
    //Requisito 2 (opcional depende de la clase)

    public long getCedula() {
        return cedula;
    }

    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    //Requisito 3:

    @Override
    public String toString() {
        return "Persona{" + "cedula=" + cedula + ", nombre=" + nombre + '}';
    }
    
    //Requisito 4:

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (int) (this.cedula ^ (this.cedula >>> 32));
        return hash;
    }

    //el método equals viene de la clase Object
    @Override
    public boolean equals(Object obj) {
        if (this == obj) { // Está preguntando si los dos objetos TIENEN la misma dirección de memoria
            return true;
        }
        if (obj == null) { // Está preguntando si el objeto que YO(this) me voy a comparar es null
            return false;
        }
        if (getClass() != obj.getClass()) { // Si los dos objetos pertenecen a la misma clase
            return false;
        }
        // el objeto a comparar se establece como "final" para que el objeto a comparar sea una constante y se asegure que NO van a cambiar el valor de sus atributos
        final Persona other = (Persona) obj; // Realizo el middlerCasting , es decir convierto el object a la clase respectiva
        if (this.cedula != other.cedula) { // Realizo la comparación SEGÚN su atributo
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Object obj) { // resta : obj1==obj2--> 0, obj1>obj2 --> ">0", obj1<obj2 --> "<0"
        
        if (this == obj) { // Está preguntando si los dos objetos TIENEN la misma dirección de memoria
            return 0;
        }
//        if (obj == null) { // Está preguntando si el objeto que YO(this) me voy a comparar es null
//            return false;
//        }
//        if (getClass() != obj.getClass()) { // Si los dos objetos pertenecen a la misma clase
//            return false;
//        }
        // el objeto a comparar se establece como "final" para que el objeto a comparar sea una constante y se asegure que NO van a cambiar el valor de sus atributos
        final Persona other = (Persona) obj; // Realizo el middlerCasting , es decir convierto el object a la clase respectiva
        /*
            ¿Cuando un objeto de la clase persona es ==, <, > ?
                REALIZO LA COMPARACIÓN POR CEDULAS
        */
            if(this.cedula==other.getCedula())
                return 0;
            if(this.cedula>other.getCedula())
                return 1;
            if(this.cedula<other.getCedula())
                return -1;
            
            
       return 0; //<-- Nunca lo va hacer 
    }
    
    
    
    
    
}
