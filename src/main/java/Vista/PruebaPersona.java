/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Vista;

import Modelo.Persona;
import javax.swing.JOptionPane;

/**
 * 
 * @author Jurgenmolina <jurgenmolina29@gmail.com>
 */
public class PruebaPersona {
    public static void main(String[] args) {
        
        try{
            Persona p1=crearPersona();
            JOptionPane.showMessageDialog(null, "Datos persona:"+p1.toString());
            Persona p2=crearPersona();
            JOptionPane.showMessageDialog(null, "Datos persona:"+p2.toString());

            Persona x= p1;
            Persona y= p2;

            //Imprimir los objetos:
            System.out.println("X:"+x.toString());
            System.out.println("Y:"+y.toString());

            //Comparando si son iguales por sus cédulas:
            if(x.equals(y))
                System.out.println(":) SI son iguales por su cédula");
            else
                System.out.println(":( NO son iguales por su cédula");

            //Comparar:
            int comparador=x.compareTo(y);

            String mensaje="Son iguales";
            switch(comparador)
            {
                case 1:{
                    mensaje="EL objeto 1 es mayor al objeto 2";
                    break;
                }
                case -1:{
                    mensaje="EL objeto 1 es menor al objeto 2";
                    break;
                }
            }
            System.out.println("Valor del comparador:"+comparador+","+mensaje);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "No se puede obtener datos (null)");
        }
    }
//        
//        Persona x=new Persona(111,"jorge");
//        Persona y=new Persona(112,"astrid");
//        
//        //Imprimir los objetos:
//        System.out.println("X:"+x.toString());
//        System.out.println("Y:"+y.toString());
//        
        //Comparando si son iguales por sus cédulas:
        
    public static Persona crearPersona()
    {
        Persona p1=new Persona();
        //Leer datos:
        String datoCedula=JOptionPane.showInputDialog(null, "Digite la cedula","Cedula:", JOptionPane.QUESTION_MESSAGE);
        
        /**
         * Validar forma antigua:
         *  if datoX No es un float
         *      --> envíe mensaje de error
         * else
         *    solicite datoY y valide de nuevo
         * 
         */
        String datoNombre=JOptionPane.showInputDialog(null, "Digite el nombre","Nombre:", JOptionPane.QUESTION_MESSAGE);
        //Clases envoltorios:
        // Si todo está bien
        try{ 
            long cedula=Long.parseLong(datoCedula);
            String nombre=datoNombre;
            p1.setCedula(cedula);
            p1.setNombre(nombre);
            return p1;
        }catch(Exception e) // Si algo pasa
        {
        
            System.err.println("Ocurrio un error en el ingreso de los datos");
        }
        return null;
    }
    
}

